We focus on bringing innovative approaches to our client’s estate planning issues in a process driven, results oriented environment. We focus exclusively on Estate Planning, Business Planning, Elder Law and Special Needs. This narrowed focus allows us to maintain a deep understanding of these areas.

Address: 7393 Hodgson Memorial Dr, #202, Savannah, GA 31406, USA

Phone: 912-352-3999

Website: https://smithbarid.com
